/**
 * Created by skiba on 2017-01-24.
 */

import com.basho.riak.client.api.RiakClient;
import com.basho.riak.client.api.commands.timeseries.Query;
import com.basho.riak.client.core.query.timeseries.QueryResult;
import com.basho.riak.client.core.query.timeseries.Row;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainRiak {
    static DateFormat dateFormat;
    static String detectorID1 = "CAUTC11FD101_D1";
    static String detectorID2 = "CAUTC11FD101_D2";
    public static void main(String[] args) throws IOException, InterruptedException {
        dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        final String query1Text = "select * from traffic3 where loop = '"+detectorID1+"' and vehicles > -1 and time > 1000000000000 and time < 1485298800000";
        final String query2Text = "select * from traffic3 where loop = '"+detectorID2+"' and vehicles > -1 and time > 1000000000000 and time < 1485298800000";
        new Plotter(getFullResultList(query1Text));  // drawing a chart vehiclesCount/date -> saved in Ztb/images
        new Plotter(getFullResultList(query2Text));
        List<Detector> query1List = getFullResultList(query1Text);
        saveQueryToFile(query1List,detectorID1);
        List<Detector> query2List = getFullResultList(query2Text);
        saveQueryToFile(query1List, detectorID2);
        int[] arr1 = getVehicleCountArray(query1List);
        int[] arr2 = getVehicleCountArray(query2List);
        correlation(arr1, arr2 , detectorID1, detectorID2);
        anomaliesDetector(query1List);
        anomaliesDetector(query2List);
    }

    private static List<Detector> getFullResultList(String queryText) {
        List<Detector> result = new ArrayList<>();
        RiakClient riakClient = null;

        System.out.println("Executing query - " + dateFormat.format(new Date()));
        try {
            riakClient = RiakClient.newClient(8087, "10.156.207.188");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        Query query = new Query.Builder(queryText).build();
        try {
            QueryResult queryResult = riakClient.execute(query);
            System.out.println("Executing query ends at - " + dateFormat.format(new Date()));
            List<Row> rowsList = queryResult.getRowsCopy();
            for (int i = 0; i < queryResult.getRowsCount(); i++) {
                String detectorID = rowsList.get(i).getCellsCopy().get(0).getVarcharAsUTF8String();
                long vehiclesCount = rowsList.get(i).getCellsCopy().get(1).getLong();
                long timestamp = rowsList.get(i).getCellsCopy().get(2).getTimestamp();
                result.add(new Detector(detectorID, vehiclesCount, timestamp));
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return result;
    }

    //    wartość współczynnika korelacji pomiędzy dowolnie wybranymi dwoma detektorami,
    public static double correlation(int[] xs, int[] ys, String det1ID, String det2ID) {
        System.out.println("Correlation compute start - " + dateFormat.format(new Date()));
        if (xs.length != ys.length) {
            throw new IllegalArgumentException();
        }
        double sx = 0.0;
        double sy = 0.0;
        double sxx = 0.0;
        double syy = 0.0;
        double sxy = 0.0;
        int n = xs.length;

        for (int i = 0; i < n; i++) {
            double x = xs[i];
            double y = ys[i];
            sx += x;
            sy += y;
            sxx += x * x;
            syy += y * y;
            sxy += x * y;
        }
        double cov = sxy / n - sx * sy / n / n;
        double sigmax = Math.sqrt(sxx / n - sx * sx / n / n);
        double sigmay = Math.sqrt(syy / n - sy * sy / n / n);
        double correlation = cov / sigmax / sigmay;
        System.out.println("Correlation compute end - " + dateFormat.format(new Date()));
        System.out.println("Correlation coefficient between "+det1ID+" and "+det2ID+" : " + correlation);
        return correlation;
    }

    public static void anomaliesDetector(List<Detector> queryList) {
        long timestampCount = 0;
        for (int i = 0; i < queryList.size() - 1; i++) {
            timestampCount += (queryList.get(i + 1).timestamp - queryList.get(i).timestamp);
        }
        long average = timestampCount / queryList.size();
        for (int i = 0; i < queryList.size() - 1; i++) {
            long diff = (queryList.get(i + 1).timestamp - queryList.get(i).timestamp);
            if (diff > average) {
                System.out.println("ANOMALY FOUND! Date : " + new Date(queryList.get(i).timestamp) + " ID : " + queryList.get(i).detectorID + " Diff : " + diff);
            } else {
                System.out.println("Reading OK! Date : " + new Date(queryList.get(i).timestamp) + " ID : " + queryList.get(i).detectorID + " Diff : " + diff);
            }
        }
    }

    private static void saveQueryToFile(List<Detector> list, String fileName) {
        try {
            PrintWriter writer = new PrintWriter("readings/" + fileName + "_" + new File("readings").listFiles().length + ".txt", "UTF-8");
            for (Detector detector : list) {
                writer.println(detector.getDetectorID() + ";" + detector.getVehiclesCount() + ";" + detector.getTimestamp());
            }
            writer.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private static int[] getVehicleCountArray(List<Detector> queryList) {
        int limit = 25000;
        int[] arr = new int[25000];
        for (int i = 0; i < limit; i++) {
            arr[i] = (int) queryList.get(i).vehiclesCount;
        }
        return arr;
    }
}


//    wykrywanie anomalii: problemy z działaniem sensora (przerwa w danych, dane nieprawdopodobne, inne).
//        result.sort(Comparator.comparing(Detector::getTimestamp));