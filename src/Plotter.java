import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by skiba on 2017-01-25.
 */
public class Plotter {
    final TimeSeries series;

    public Plotter(List<Detector> list) throws IOException {
        this.series = new TimeSeries("Data", Millisecond.class);

        for (Detector detector : list) {
            Timestamp timeStmp = new Timestamp(detector.getTimestamp());
            try {
                long value = detector.vehiclesCount;
                series.add(new Millisecond(timeStmp), value);
            } catch (SeriesException e) {
                System.err.println("Error adding to series");
            }
        }
        final XYDataset dataset = new TimeSeriesCollection(series);
        printChart(dataset,"Date","Vehicles count", list.get(0).getDetectorID());
    }

    private void printChart(XYDataset dataset, String xAxisTitle, String yAxisTitle, String mainTitle) throws IOException {
        JFreeChart timechart = ChartFactory.createTimeSeriesChart(mainTitle, xAxisTitle, yAxisTitle, dataset, false, false, false);

        int width = 800; /* Width of the image */
        int height = 600; /* Height of the image */
        File timeChart = new File("images/chart" + new File("images").listFiles().length + ".jpeg");
        ChartUtilities.saveChartAsJPEG(timeChart, timechart, width, height);
    }
}
