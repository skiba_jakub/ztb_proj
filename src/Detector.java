/**
 * Created by skiba on 2017-01-24.
 */
public class Detector {
    String detectorID;
    long vehiclesCount;
    long timestamp;

    public Detector(String detectorID, long vehiclesCount, long timestamp) {
        this.detectorID = detectorID;
        this.vehiclesCount = vehiclesCount;
        this.timestamp = timestamp;
    }

    public String getDetectorID() {
        return detectorID;
    }

    public long getVehiclesCount() {
        return vehiclesCount;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
